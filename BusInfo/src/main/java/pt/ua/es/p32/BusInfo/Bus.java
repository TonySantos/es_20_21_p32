package pt.ua.es.p32.BusInfo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)

public class Bus {

    private String id;
    private String node_id;
    private String location_id;
    private String head;
    private String lon;
    private String lat;
    private String speed;
    private String ts;
    private String write_time;

    public Bus() {
    }

    public Bus(String[] values) {
        this.id = values[0];
        this.node_id = values[1];
        this.location_id = values[2];
        this.head = values[3];
        this.lon = values[4];
        this.lat = values[5];
        this.speed = values[6];
        this.ts = values[7];
        this.write_time = values[8];

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNode_id() {
        return node_id;
    }

    public void setNode_id(String node_id) {
        this.node_id = node_id;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getWrite_time() {
        return write_time;
    }

    public void setWrite_time(String write_time) {
        this.write_time = write_time;
    }

    @Override
    public String toString() {
        return "Bus [head=" + head + ", id=" + id + ", lat=" + lat + ", location_id=" + location_id + ", lon=" + lon
                + ", node_id=" + node_id + ", speed=" + speed + ", ts=" + ts + ", write_time=" + write_time + "]";
    }

}
