package pt.ua.es.p32.BusInfo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// This will be AUTO IMPLEMENTED by Spring into a Bean called weatherRepository
// CRUD refers Create, Read, Update, Delete
@Repository
public interface BusRepository extends CrudRepository<Bus, Integer> {

}