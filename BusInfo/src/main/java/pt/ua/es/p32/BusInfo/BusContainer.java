package pt.ua.es.p32.BusInfo;

public class BusContainer{
    private Bus b;

    public BusContainer(){
    }

    public void setBusContainer(Bus b){
        this.b = b;
    }

    public Bus getBusContainer(){
        return b;
    }
}