package pt.ua.es.p32.BusInfo;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import   java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;


@SpringBootApplication
@EnableScheduling
public class BusInfoApplication {
    private static final Logger log = LoggerFactory.getLogger(BusInfoApplication.class);

    @Autowired
    private KafkaSender sender;

    //@Autowired
    //private BusRepository busRepo;

    @Autowired
    private BusContainer busContainer;

    public static void main(String[] args){
        SpringApplication.run(BusInfoApplication.class, args);
    }

    public void readCSV() throws IOException, URISyntaxException {
        List<Bus> records = new ArrayList<Bus>();
        Resource resource = new ClassPathResource("test1.csv");

        try {
            System.out.println(resource.getInputStream());
            CSVReader csvReader = new CSVReader(new InputStreamReader(resource.getInputStream()));
            String[] values;
            while ((values = csvReader.readNext()) != null) {
                Bus tmp = new Bus(values);
                records.add(tmp);
                sender.send(tmp.toString());
                busContainer.setBusContainer(tmp);
                //busRepo.save(tmp);

            }
        } catch (IOException ex) {
            log.error("ERROR!");
        }
    }

    @Bean
    @Scope("singleton")
    public BusContainer busContainer(){
        return new BusContainer();
    }

    @Scheduled(fixedRate = 10000)
    public void update_rest()  throws IOException, URISyntaxException{
        readCSV();
    }
}
