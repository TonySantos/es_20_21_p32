package pt.ua.es.p32.BusInfo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import pt.ua.es.p32.BusInfo.*;

@Controller
public class WebController {

    private  List<Bus> records;
    private static final Logger log = LoggerFactory.getLogger(WebController.class);
    @GetMapping("/history")
    public String history(@RequestParam(required=true, defaultValue="24582934")String busId, Model model)  throws IOException, URISyntaxException {
        readCSV();
        /*for(int i=0; i< records.size();i++){
            if(records.get(i).getId().equals(busId)){
                model.addAttribute("Bus_Id",records.get(i).getId());
                model.addAttribute("Longitude", records.get(i).getLon());
                model.addAttribute("Latitude", records.get(i).getLat());
                model.addAttribute("Speed", records.get(i).getSpeed());
                model.addAttribute("Time", records.get(i).getTs());
            }
        }*/
        model.addAttribute("buslist", records);
        return "history";
    }

    /*@GetMapping("/mappage")
    public String mappage(@RequestParam(required=true, defaultValue="24582934")String busId, Model model2) throws IOException, URISyntaxException {
        //for(int i=0; i< records.size();i++){
            readCSV();
           // model2.addAttribute("Bus_Id",records.get(2).getId());
            model2.addAttribute("Longitude", records.get(1).getLon());
            model2.addAttribute("Latitude", records.get(1).getLat());
            //model2.addAttribute("Speed", records.get(2).getSpeed());
            //model2.addAttribute("Time", records.get(2).getTs());
            //}
        //}
        return "mappage";
    }*/

    @RequestMapping(path = "/mappage", method = RequestMethod.GET)
    public String getUsers(Model model) throws IOException, URISyntaxException {
        readCSV();
        model.addAttribute("buslist", records);
    return "mappage";
    }


    public void readCSV() throws IOException, URISyntaxException {
        records = new ArrayList<Bus>();
        Resource resource = new ClassPathResource("test1.csv");

        try {
            System.out.println(resource.getInputStream());
            CSVReader csvReader = new CSVReader(new InputStreamReader(resource.getInputStream()));
            String[] values;
            while ((values = csvReader.readNext()) != null) {
                Bus tmp = new Bus(values);
                records.add(tmp);

            }
        } catch (IOException ex) {
            System.out.print("ERRO");
        }
        System.out.println(records);
    }

    //@GetMapping("/all")
    //public @ResponseBody Iterable<Bus> getAllBus(){
      //  return BusRepository.findAll();
    //}
}
