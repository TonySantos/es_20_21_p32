package pt.ua.es.p32.BusInfo;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.kafka.clients.consumer.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Properties;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Value;

@RunWith(SpringRunner.class)
@SpringBootTest
class BusInfoApplicationTests {
    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;
    
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Test
    public void KafkaTest() throws Exception {
        kafkaTemplate.send("esp32Test", "Sending with own simple KafkaProducer");
        KafkaConsumer consumer = createConsumer();
        ConsumerRecords<String,String> records=consumer.poll(100); 
        
        assertEquals(records.count(), 1);
    }

    private KafkaConsumer<String, String> createConsumer() {
        Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaExampleConsumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        // Create the consumer using props.
        KafkaConsumer<String,String> consumer= new KafkaConsumer<String,String>(props); 

        // Subscribe to the topic.
        consumer.subscribe(Arrays.asList("esp32Test"));
        return consumer;
    }
}
